console.log("DOM MANIPULATION")

console.log(document.getElementById("demo"))
console.log(document.getElementsByTagName("h1"))
console.log(document.getElementsByClassName("title"))

console.log(document.querySelector(".title"))
console.log(document.querySelector("h1"))
console.log(document.querySelector("#demo"))

let myH1 = document.querySelector("h1");
myH1.innerHTML = "hello world"

document.getElementById("demo").setAttribute("class", "sample")
document.getElementById("demo").removeAttribute("class")

document.querySelector(".title").style.color = "red"

let firstName = document.querySelector("#txt-first-name");
let lastName = document.querySelector("#txt-last-name")
let fullName = document.getElementById("span-full-name")


const updateName = () => {
	let txtName = firstName.value
	let txtLast = lastName.value

	fullName.innerHTML = `${txtName} ${txtLast}`
}

firstName.addEventListener("keyup", updateName)

lastName.addEventListener("keyup", updateName)